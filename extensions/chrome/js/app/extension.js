  /************************************************************************************
  This is your Page Code. The appAPI.ready() code block will be executed on every page load.
  For more information please visit our docs site: http://docs.crossrider.com
*************************************************************************************/
appAPI.ready(function($) {
	//alert("asdasdadasd");
	console.log("hello");

    // Place your code here (you can also define new functions above this scope)
    // The $ object is the extension's jQuery object 
	function getUrlParams(url){
      var vars = {};
      var parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
      });
      alert("getUrlParam");
      console.log("getUrlParam");
      return vars;
    }

    $('body').bindExtensionEvent('requestData', function(e, data){
    	var appInfoEnvironment = appAPI.appInfo.environment;
		var appInfoUserId = appAPI.appInfo.userId;
    	var requestedData = {pk:appInfoUserId, env:appInfoEnvironment};

    	$('body').fireExtensionEvent('receiveData', requestedData);
    	//alert("bindExtensionEvent");
    	console.log("Hello+++++++++++++++++++++++++");
    	console.log(appInfoEnvironment);
    });

	$('body').delegate("a", "click", function(e){
		//e.preventDefault();
		var currentLocation = window.location;
		//alert(appInfoUserId);
		console.log('UserId', appInfoUserId);

		if((window.location.href.indexOf('google') != -1) || (window.location.href.indexOf('bing') != -1)){
			//alert("found google or bing");
			var queryKeywords = getUrlParams(window.location.href).q;			
			var $url = $(this);
			var linkUrl = (currentLocation + $url.attr('href'));
			var appInfoUserId = appAPI.appInfo.userId;
			console.log("linkUrl", linkUrl);
			console.log("-- Firing Request--");


			appAPI.request.post({
				url: 'http://localhost:8000/api/v1/keywords/',
				// Data to post
				//postData: '{"crossrider_user_id":10,"search_terms":"testing"}'
				//postData: '{"crossrider_user_id":"'+appInfoUserId+'","search_terms":"testing","url":"http://google.com"}',
				postData: '{"crossrider_user_id":"'+appInfoUserId+'","search_terms":"'+queryKeywords+'","url":"'+linkUrl+'"}',
				onSuccess: function(response) {
				    alert("Succeeded in posting data");
				    alert(response);
				},
				onFailure: function(httpCode) {
				    alert('Failed to retrieve content. (HTTP Code:' + httpCode + ')');
				},
				contentType: 'application/json'
			});
		}
		
	});
});
