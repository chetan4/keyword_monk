from django.contrib import admin
from .models import *

class Geographical_info_admin(admin.ModelAdmin):
	"""this class will admin geograohical ifnormation related to a query"""
	pass

class Crossrider_app_admin(admin.ModelAdmin):
	fields =('user_id','crossrider_app_id','crossrider_user_id','crossrider_app_info') #to be shown inside A record
	list_filter = ('user_id','crossrider_user_id','created_at',) #to be show on rhs for filetring 
	search_fields = ["crossrider_user_id","crossrider_app_info",] #fields used for searchings
	list_display = ["user_id","crossrider_user_id",'created_at',] #tobe shown where paginated records are shown

class Queries_admin(admin.ModelAdmin):
	fields = ('crossrider_user_id','search_terms','geographical_inforamation','url',)
	list_filter = ('deleted','crossrider_user_id','created_at',)
	search_fields = ['search_terms',]
	list_display = ['search_terms','crossrider_user_id','created_at',]

class Browsing_history_admin(admin.ModelAdmin):
	fields = ('crossrider_user_id','url','geographical_inforamation','tab_event','tab_id')
	list_filter = ('deleted','crossrider_user_id','created_at','tab_event',)
	search_fields = ['url',]
	list_display = ['id','crossrider_user_id','created_at','tab_event','tab_id']

admin.site.register(Geographical_info,Geographical_info_admin)
admin.site.register(Crossrider_app,Crossrider_app_admin)
admin.site.register(Queries,Queries_admin)
admin.site.register(Browsing_history,Browsing_history_admin)

