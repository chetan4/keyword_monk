from tastypie import fields, utils
from tastypie.resources import ModelResource
from tastypie.authentication import SessionAuthentication
from tastypie.authorization import DjangoAuthorization, Authorization
from .models import *
from django.contrib.auth.models import User
import logging
from django.db import IntegrityError

logger = logging.getLogger(__name__) #make a logger in name of module

class Geographical_info_resource(ModelResource):
	class Meta:
		queryset = Geographical_info.objects.all()
		resource_name = "locations"
		allowed_methods = ['post']
		#authentication = SessionAuthentication()#user auth
		#authorization = DjangoAuthorization()#permissions
		authorization = Authorization()


class User_resource(ModelResource):

	crossrider_user_ids = fields.ToManyField('apis.api.Crossrider_app_resource','crossrider_user_ids',related_name='users',null=True)
	class Meta:
		queryset = User.objects.all()
		resource_name = "user"
		excludes = ["password",'deleted','email']
		allowed_methods = ['get']

	def authorized_read_list(self, object_list, bundle):
		return object_list.filter(id=bundle.request.user.id)


class Crossrider_app_resource(ModelResource):
	user_id = fields.ForeignKey('apis.api.User_resource','users',full=True,null=True)
	class Meta:
		queryset = Crossrider_app.objects.all()
		resource_name = "users"
		allowed_methods = ['get','post']
		excludes = ['user_id','created_at','deleted',]
		#authentication = SessionAuthentication()
		#authorization = DjangoAuthorization()
		authorization = Authorization()

	def obj_create(self, bundle, **kwargs):
		try:
			logger.info(bundle.request.user)
			userid = bundle.request.user.id

			if userid is not None:
				bundle.obj.user_id = User.objects.get(pk=userid)
				bundle.obj.crossrider_user_id =  bundle.data["crossrider_user_id"]
				logger.info("just before saving into db")
				bundle.obj.save()
			else:
				logger.info("Attempt by a Anonymous user to create a crossrider user using api")

		except IntegrityError:
			logger.info('Oh damn integrity error')
		return bundle

	def authorized_read_list(self, object_list, bundle):
		return object_list.filter(id=bundle.request.user.id)

class Queries_resource(ModelResource):
	class Meta:
		queryset = Queries.objects.all()
		resource_name = "keywords"
		allowed_methods = ['post']
		excludes = ['created_at','deleted',]
		#authentication = SessionAuthentication()
		#authorization = DjangoAuthorization()
		authorization = Authorization()

	def authorized_read_list(self, object_list, bundle):
		return object_list.filter(id=bundle.request.user.id)

class Browsing_history_resource(ModelResource):
	class Meta:
		queryset = Browsing_history.objects.all()
		resource_name = "urls"
		allowed_methods = ['post','get']
		excludes = ['created_at','deleted',]
		#authentication = SessionAuthentication()
		#authorization = DjangoAuthorization()
		authorization = Authorization()

	def authorized_read_list(self, object_list, bundle):
		return object_list.filter(id=bundle.request.user.id)