import logging

logger = logging.getLogger('access')

"""This logs every request and response"""

class RequestLoggerMiddleware(object):
	def process_request(self, request):
		logger.info("Request")
		if request.POST is not None:
			logger.info("POST")
			logger.info(request.POST)
		if request.GET is not None:
			logger.info("GET")
			logger.info(request.GET)
		logger.info("Body")
		try:
			logger.info(body)
		except:#for any exception in logger we dont want to cause 500 page.
			logger.info("[URGENT]logger caused some error")
		logger.info("++++++++++++++++++++++++++++")
		return None

