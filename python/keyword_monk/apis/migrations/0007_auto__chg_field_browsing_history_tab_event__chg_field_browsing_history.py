# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Browsing_history.tab_event'
        db.alter_column(u'apis_browsing_history', 'tab_event', self.gf('django.db.models.fields.CharField')(max_length=256, null=True))

        # Changing field 'Browsing_history.tab_id'
        db.alter_column(u'apis_browsing_history', 'tab_id', self.gf('django.db.models.fields.CharField')(max_length=2056, null=True))

    def backwards(self, orm):

        # Changing field 'Browsing_history.tab_event'
        db.alter_column(u'apis_browsing_history', 'tab_event', self.gf('django.db.models.fields.CharField')(max_length=256))

        # Changing field 'Browsing_history.tab_id'
        db.alter_column(u'apis_browsing_history', 'tab_id', self.gf('django.db.models.fields.CharField')(max_length=2056))

    models = {
        u'apis.browsing_history': {
            'Meta': {'object_name': 'Browsing_history'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {}),
            'crossrider_user_id': ('django.db.models.fields.CharField', [], {'max_length': '2056'}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'geographical_inforamation': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['apis.Geographical_info']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tab_event': ('django.db.models.fields.CharField', [], {'default': "'Open'", 'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'tab_id': ('django.db.models.fields.CharField', [], {'default': "'None'", 'max_length': '2056', 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '2056', 'blank': 'True'})
        },
        u'apis.crossrider_app': {
            'Meta': {'object_name': 'Crossrider_app'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'crossrider_app_id': ('django.db.models.fields.IntegerField', [], {'default': "'28698'", 'blank': 'True'}),
            'crossrider_app_info': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'crossrider_user_id': ('django.db.models.fields.CharField', [], {'max_length': '2056'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'apis.geographical_info': {
            'Meta': {'object_name': 'Geographical_info'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'apis.queries': {
            'Meta': {'object_name': 'Queries'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {}),
            'crossrider_user_id': ('django.db.models.fields.CharField', [], {'max_length': '2056'}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'geographical_inforamation': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['apis.Geographical_info']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'search_terms': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '2056', 'blank': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['apis']