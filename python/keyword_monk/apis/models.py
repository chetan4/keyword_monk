from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
import logging

logger = logging.getLogger(__name__) #make a logger in name of module

class Geographical_info(models.Model):
	"""Yet to come and more thinking is neededs"""

	class Meta:
		verbose_name_plural = "geographical information"

	def __unicode__(self):
		return "Geographical Info" 


class Crossrider_app(models.Model):
	"""this class will store crossrider user id --> our user id reltion"""
	user_id = models.ForeignKey(User)#many to one relationship between crorrider--> userid
	crossrider_app_id = models.IntegerField(blank=True,default='28698')
	crossrider_user_id = models.CharField(max_length=2056)
	crossrider_app_info = models.TextField(blank=True, null=True)
	created_at = models.DateTimeField(editable=False, null=True)

	class Meta:
		verbose_name_plural = "crossrider registrations"

	def save(self):
		"""update created_at with current datetime while saving"""
		logger.info('inside Crossridr_app model>save function')
		self.created_at = datetime.now()
		super(Crossrider_app,self).save()

	def __unicode__(self):
		return unicode(self.user_id.id)

class Queries(models.Model):
	"""actuall queries fired are saved here"""
	crossrider_user_id = models.CharField(max_length=2056)
	search_terms = models.TextField(blank=True)
	created_at = models.DateTimeField(editable=False)
	url = models.URLField(max_length=2056,null=False,blank=True)
	geographical_inforamation = models.OneToOneField(Geographical_info,blank=True, null=True)
	deleted = models.BooleanField()

	class Meta:
		verbose_name_plural = "search terms"

	def save(self):
		"""update created_at with current datetime while saving"""
		self.created_at = datetime.now()
		super(Queries,self).save()

	def __unicode__(self):
		return  unicode(self.search_terms)

class Browsing_history(models.Model):
	"""actuall queries fired are saved here"""
	OPEN = "OP"
	CLOSE = "CL"
	TAB_EVENTS = (
        (OPEN, 'Open'),
        (CLOSE, 'Close'),
    )
	crossrider_user_id = models.CharField(max_length=2056)
	created_at = models.DateTimeField(editable=False)
	url = models.URLField(max_length=2056,null=False,blank=True)
	geographical_inforamation = models.OneToOneField(Geographical_info,blank=True, null=True)
	tab_event = models.CharField(max_length=256,default='Open',choices=TAB_EVENTS,blank=True, null=True)
	tab_id = models.CharField(max_length=2056,default="None",blank=True, null=True)
	deleted = models.BooleanField()

	class Meta:
		verbose_name_plural = "browsing history"

	def save(self):
		"""update created_at with current datetime while saving"""
		self.created_at = datetime.now()
		super(Browsing_history,self).save()

	def __unicode__(self):
		return  unicode(self.url)


