from django.db import models
from registration.signals import user_registered
from apis.models import Crossrider_app
from django.dispatch import receiver
import logging

logger = logging.getLogger(__name__)

#this is always loaded first so all frontend signal handling will happen here

@receiver(user_registered)
def create_crossrider_to_user_id_relationship(sender, user, request, **kwargs):
	logger.info('Adding new user in crossrider_app')
	new_user = Crossrider_app(user_id=user,crossrider_user_id=request.session["crossrider_user_id"])
	new_user.save()	

#post_save.connect(create_crossrider_to_user_id_relationship, sender=user_registered, dispatch_uid="users-registered-signal")
