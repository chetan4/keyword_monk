from .models import * #so that signals work
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
import datetime
from django.contrib.auth.decorators import login_required
from apis.models import Queries, Browsing_history
from django.core.urlresolvers import reverse

def verify(request):
	return render(request, 'dynamic_pages/verify.html', {})

def after_verify(request):
	request.session["crossrider_user_id"] = request.GET.get("crossrider_user_id")
	return HttpResponseRedirect(reverse("registration_register"))

def terms(request):
	return render(request,'flatpages/terms.html',{})

def privacy_policy(request):
	return render(request,'flatpages/privacy_policy.html',{})

def contact(request):
	return render(request,'flatpages/contact.html',{})

def home(request):
	data_size = Browsing_history.objects.count()
	data_size += Queries.objects.count()
	return render(request, 'dynamic_pages/home.html', {"worth":data_size})
