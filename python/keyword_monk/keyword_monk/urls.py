from django.conf.urls import patterns, include, url
from tastypie.api import Api
from apis.api import Crossrider_app_resource, Queries_resource, User_resource, Browsing_history_resource
from frontend.views import *
from django.contrib.auth.views import login, logout

v1_api = Api(api_name="v1")
v1_api.register(Crossrider_app_resource())
v1_api.register(Queries_resource())
v1_api.register(User_resource())
v1_api.register(Browsing_history_resource())

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'keyword_monk.views.home', name='home'),
    # url(r'^keyword_monk/', include('keyword_monk.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^api/', include(v1_api.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^verify/$', verify, name="frontend_verify"),
    url(r'^after_verify/$', after_verify, name="frontend_after_verify"),
    url(r'^terms/$', terms, name="frontend_terms"),
    url(r'^privacy_policy/$', privacy_policy, name="frontend_privacy_policy"),
    url(r'^contact/$',contact, name="frontend_contact"),
    url(r'^$',home, name="frontend_home" ),
    url(r'^accounts/', include('registration.urls')),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', name="frontend_login"),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'template_name': 'registration/logout.html'}, name="frontend_logout"),
    )   