// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var KM = KM || {};

KM = (function($, window, document, KeywordMonk){
	KeywordMonk.Invites = {
		activateAjaxInvite: function(){
			$('.marketing').delegate('.business-invites-form', 'submit', function(e){
				e.preventDefault();
				
				$.ajax({
					url: $(this).attr('action'),
					type: 'post',
					data: $(this).serialize(),
					beforeSend: function(){
						$('.errors').removeClass('alert').removeClass('alert-error');
						$('.errors').html("");
					},
					success: function(response){
						if(response.status == 'success'){
							$('.business-invites-form').hide();
							$('.marketing-title').after('<p> Thanks! We\'ll notify you when we launch </p>');
						}else{							
							$('.errors').addClass('alert').addClass('alert-error');
							$('.errors').html("<strong> Oops </strong>" + response.errors);
						}

					}
				});
			});
		}
	}

//	Document Ready
	$(function(){
		if($('.business-invites-form').length > 0){			
			KeywordMonk.Invites.activateAjaxInvite();
		}

	});

	return KeywordMonk;
})(jQuery, this, this.document, KM);