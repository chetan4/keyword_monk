// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
var KM = KM || {};

KM = (function($, window, document, KeywordMonk){
	var getPluginKey = function(){
		var pageUrl = window.location.href;
		var pageUrlParameters = KeywordMonk.Utilities.getUrlParams(pageUrl);
		return pageUrlParameters.pk;
	}

	KeywordMonk.Users = {
		addPluginKeyToForm: function(){
			var pluginKey = getPluginKey();	
			if ((pluginKey != null) && (pluginKey != undefined) && (pluginKey.trim() != ""))
				$('#plugin_key').val(pluginKey);
		}
	}

	// document ready
	$(function(){
		if($('#new_user').length > 0){
			KeywordMonk.Users.addPluginKeyToForm();
		}
	});

	return KeywordMonk;
})(jQuery, this, this.document, KM);