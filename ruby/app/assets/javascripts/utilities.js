var KM = KM || {};


KM = (function($, window, document, KeywordMonk){
	KeywordMonk.Utilities = {
		getUrlParams: function(url){
			var vars = {};
    		var parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        		vars[key] = value;
      		});
    		return vars;		
		}
	}

	return KeywordMonk;
})(jQuery, this, this.document, KM);

