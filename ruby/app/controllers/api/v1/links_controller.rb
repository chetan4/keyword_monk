class Api::V1::LinksController < ApplicationController  
	protect_from_forgery :except => [:create]
	respond_to :json, :xml
	
  	def index
      @links = Link.all
  	end

  	def create                
      @uri = UrlParser.new(params[:url]).parse_uri      
      @link = Link.new(:url => @uri.target_url, :query_string => @uri.query, :domain => @uri.domain,
                       :port => @uri.port, :path => @uri.path, :fragment => @uri.fragment, :referrer => @uri.referrer, 
                       :referrer_querystring => @uri.referrer_query, :full_url => @uri.full_url)  

      @keyword = @link.keywords.new(:words => URI.unescape(params[:keywords]))      
      @user = User.where(:plugin_key => params[:userId]).last
      @link.user = @user if @user.present?

  		respond_to do |format|       
  			if @link.save  			
  				format.json {
            render :json => {:status => :ok, :link => @link}.to_json
          }

          format.xml{
            render :xml => {:status => :ok, :link => @link}.to_xml
          }
  			else
  				format.json {
            render :json => {:status => :ok, :errors => @link.errors}.to_json
          }

          format.xml {
            render :xml => {:status => :ok}.to_xml
          }
  			end
  		end

    rescue URI::InvalidURIError => e
      respond_to do |format|
        format.json { render :json => {:status => :bad_request}}
      end
    rescue => e
      respond_to do |format|
        Rails.logger.error("[ERROR] #{e.message} \ #{e.backtrace}")
        format.json { render :json => {:status => :unprocessable_entity}}
      end
  	end

  	def destroy
  	end
end
