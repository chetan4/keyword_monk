class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :current_user

  def current_user
  	warden.user if warden.present?
  end 

  def warden
  	env['warden']
  end
end
