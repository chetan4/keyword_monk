class Business::InvitesController < ApplicationController
  def new
  	render :layout => 'layouts/downloads_layout'
  end

  def create 
  	email = params[:email]  	
  	invites_api = Services::Invite.new(email)
  	response = invites_api.create
  	respond_to do |format|  		
	  	if response['status'] == 'success'
	  		format.json {
  				render :json => {:status => 'success'}	
  			}
  			format.html{}
  		elsif response['status'] == 'error' && response['errors']['email'].include?("has already been taken")
  			format.json {
  				render :json => {:status => 'success'}	
  			}
  			format.html{}
  		else
  			format.json {
  				render :json => {:status => 'error', :errors => "#{response['errors'].keys.first} #{response['errors'].values.join(', ')}" }
  			}
  			format.html{}
	  	end
	  end
  end
end
