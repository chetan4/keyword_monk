class Business::UsersController < ApplicationController
  def new
  end

  def register
  	@business_user = BusinessUser.new
  end

  def create
  	binding.pry
  	@business_user = BusinessUser.new(params[:business_user])
  	if @business_user.save
  		flash[:notice] = "Account has been successfully created."
  		warden.set_user(@business_user)
  		redirect_to dashboard_business_user_url(@business_user)
  	else
  		flash.now[:error] = "Please fix the following errors to complete the account creation process."
  		render :register
  	end
  end

  def dashboard
  end
end
