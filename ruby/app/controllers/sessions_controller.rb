class SessionsController < ApplicationController
  def new
  end

  def create
  	user = warden.authenticate!
  	if user  		
  		redirect_to api_v1_links_url
  	else
  		flash[:error] = "Invalid username or password"
  		render "new"
  	end
  end

  def destroy      
  	warden.logout if warden.user.present?
  	redirect_to register_api_v1_users_url
  end
end
