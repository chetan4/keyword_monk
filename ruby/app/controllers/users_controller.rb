class UsersController < ApplicationController
  def new
    @user = User.new
    render :register
  end

  def register
    @user = User.new    
  end

  def create    
    @user = User.new(params[:user])
    @user.plugin_key = params[:plugin_key] if params[:plugin_key].present?
    @pk = params[:plugin_key]    
    @user.role = "PLUGIN"    

    if @user.save            
      warden.set_user(@user)
      redirect_to root_url, :notice => "Sign up was successful"
    else
      flash.now[:error] = "Sign up could not be completed"
      render :register
    end
  end

  def verify_installation    
  end

  def edit
  end

  def update
  end

  def show
  end

  def destroy
  end
end
