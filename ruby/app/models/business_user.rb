class BusinessUser < User
	self.inheritance_column = :_type_disabled

	def self.new(params=nil)
		user = super(params)
		user.role = "BUSINESS"
		user
	end
end