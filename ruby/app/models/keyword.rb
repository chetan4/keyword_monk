# == Schema Information
#
# Table name: keywords
#
#  id         :integer          not null, primary key
#  words      :text
#  link_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Keyword < ActiveRecord::Base
	#associations
	belongs_to :link

	#validations
	validates_presence_of :words	
	attr_accessible :words
end
