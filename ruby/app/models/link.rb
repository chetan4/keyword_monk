# == Schema Information
#
# Table name: links
#
#  id                   :integer          not null, primary key
#  url                  :text
#  path                 :text
#  referrer             :string(255)
#  referrer_querystring :text
#  domain               :string(255)
#  fragment             :string(255)
#  port                 :string(255)
#  query_string         :text
#  full_url             :text
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  user_id              :integer
#

class Link < ActiveRecord::Base
  #associations
  has_many :keywords, :dependent => :destroy
  belongs_to :user

  #validations
  validates_presence_of :url, :path, :domain

  #mass assignment protection
  attr_accessible :url, :path, :referrer, :referrer_querystring, :domain, :fragment, :port, 
                  :query_string, :full_url


  
  
  
   
end
