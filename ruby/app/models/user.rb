# == Schema Information
#
# Table name: users
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  encrypted_password :string(255)
#  email              :string(255)      default(""), not null
#  plugin_key         :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  salt               :string(255)
#  roles_mask         :integer
#

class User < ActiveRecord::Base
    #include RoleModel

    #constants
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  	attr_accessor :password, :password_confirmation
  	attr_accessible :email, :name, :password, :password_confirmation		

    #associations
    has_many :links

  	#validations
  	validates :email, :presence => true,
  					  :format => {:with => VALID_EMAIL_REGEX}
    validates_uniqueness_of :email, :scope => :role
    validates_presence_of :role  			

  	validates_presence_of :password, :password_confirmation, :on => :create
  	validates_presence_of :name
    validates_presence_of :plugin_key, :message => "Please ensure plugin is installed and enabled and refersh this page.", :if => Proc.new {|user| user.role == 'PLUGIN'}
    validates_uniqueness_of :plugin_key, :scope => :email, :if => Proc.new{|user| user.role == 'PLUGIN'}

  	validate :password_matches_confirmation?, :on => :create  	

    #hooks
    before_create :generate_crypted_password

    # plugin user, business user and admin user

  	def password_matches_confirmation?
  		self.password == self.password_confirmation
  	end

  	def generate_crypted_password
  		self.salt = ::BCrypt::Engine.generate_salt(10)
  		self.encrypted_password = encrypt_password(self.password, self.salt)
  	end

  	def authenticate
  		(self.encrypted_password == encrypt_password(self.password, self.salt))
  	end

  private  

  	def encrypt_password(password, salt)  	
  		encrypted_password = ::BCrypt::Engine.hash_secret(self.password, salt)  	  	
  	end
end
