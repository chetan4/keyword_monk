Rails.application.config.middleware.use Warden::Manager do |manager|	
	manager.default_strategies :password, :business
	manager.failure_app = lambda {|env| SessionsController.action(:new).call(env) }	
end


Warden::Strategies.add(:password) do
  def valid?
    params['email'] || params['password']
  end

  def authenticate!
    user = User.where(:email => params['email'], :role => "PLUGIN")
    if user && user.authenticate(params['password'])
    	success! user
    else    	
    	fail!("Invalid email or password")
 	  end
  end
end

Warden::Strategies.add(:business) do
  def valid?
    params['email'] || params['password']
  end

  def authenticate!
    user = User.where(:email => params['email'], :role => "BUSINESS")
    if user && user.authenticate(params['password'])
      success! user
    else      
      fail!("Invalid email or password")
    end
  end
end


Warden::Manager.serialize_into_session do |user|
  	user.id
end

Warden::Manager.serialize_from_session do |id|
	User.find(id)
end

