class CreateKeywords < ActiveRecord::Migration
  def change
    create_table :keywords do |t|
      t.text :words      
      t.integer :link_id
      t.timestamps
    end
  end
end
