class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.text :url
      t.text :path
      t.string :referrer
      t.text :referrer_querystring
      t.string :domain
      t.string :fragment
      t.string :port
      t.text :query_string      
      t.text :full_url
      t.string :domain
      t.timestamps
    end
  end
end
