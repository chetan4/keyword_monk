class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :encrypted_password
      t.string :email, :null => false, :default => ""
      t.string :plugin_key

      t.timestamps
    end
  end
end
