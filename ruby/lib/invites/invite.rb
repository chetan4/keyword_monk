require 'net/http'
module Services	
	class Invite
		attr_accessor :email, :invitation_token, :created_at, :updated_at, :id
		attr_reader :host, :port		

		def initialize(email)
			@email = email			
		end

		def create						
			data 	 = {:email => self.email}.to_json						
			response = PostRequest.new(::INVITES_API, "/invites", {}, data, {'Content-Type' => 'application/json'}).send_request
			@response = Invite.parse_response(response)
		end

		def self.find_invite_by_invitation_token(invitation_token)
			raise ArgumentError if invitation_token.blank? || invitation_token.nil?			
			response = GetRequest.new(::INVITES_API, "/invite/by_token/" + invitation_token, {}).send_request
			@response = parse_response(response)
		end

		def self.all(page = 1, per_page = 50, offset = nil)			
			response = GetRequest.new(::INVITES_API, "/invites", {'page' => page, 'per_page' => per_page, 'offset' => offset}).send_request
			@response = parse_response(response)
		end

		def self.send_invites(limit, order = "1", resend = 'false')			
			response = PutRequest.new(::INVITES_API, "/invites/send_invites", {:limit => limit, :order => order, :resend => resend}).send_request			
			@response = parse_response(response)
		end	

		private

		def self.parse_response(response)
			JSON.parse(response.body)
		end
	end
end