require 'net/http'
module Services
	class Request		
		attr_accessor :host, :path, :params, :data, :request, :headers

		def initialize(host, path, params, data = nil, headers = {})
			@host = host
			@path = path
			@params = params
			@data = data
			@headers = headers 
		end

		def send_request	
			self.make_request
		end

		def get_http(full_uri)
			uri = URI.parse(full_uri)
			http = Net::HTTP.new(uri.host, uri.port)			
		end

		def params_to_querystring
			query_string = []
			self.params.each do |key, value|
				query_string << "#{key.to_s}=#{value}"
			end

			query_string.join('&')
		end

		def parse_response(response)
			JSON.parse(response)
		end

		def set_headers
			self.headers.each do |k, v|
				@request["#{k.to_s}"] = v.to_s
			end			
		end

		def set_body
			@request.body = self.data
		end
	end

	class GetRequest < Request
		def make_request
			full_uri = self.host.to_s + self.path.to_s
			http = get_http(full_uri)	
			params = params_to_querystring			
			path_with_params = self.path.to_s + "?" + params
			@request = Net::HTTP::Get.new(path_with_params)		
			http.request(@request)
		end
	end

	class PostRequest < Request
		def make_request
			full_uri = self.host.to_s + self.path.to_s
			http = get_http(full_uri)
			@request = Net::HTTP::Post.new(self.path.to_s)
			set_headers
			set_body
			http.request(@request)
		end
	end

	class PutRequest < Request
		def make_request
			full_uri = self.host.to_s + self.path.to_s
			http = get_http(full_uri)			
			params = params_to_querystring
			path_with_params = self.path.to_s + "?" + params
			@request = Net::HTTP::Put.new(path_with_params)
			set_headers
			http.request(request)
		end
	end

end