class UrlParser
	attr_accessor :full_url, :parsed_url
	attr_reader :domain, :path, :query, :target_url, :fragment, :port, :referrer, :referrer_query

	def initialize(full_url)
		@full_url = full_url
	end

	def parse_uri
		escaped_url 	  = URI.escape(full_url)
		@parsed_url 	  = URI.parse(escaped_url)
		@referrer 		  = parsed_url.host
		@referrer_query   = parsed_url.query
		@target_url 	  = parse_target_url
		parsed_target_url = URI.parse(@target_url)
		@domain 		  = parsed_target_url.host
		@port			  = parsed_target_url.port
		@path			  = parsed_target_url.path
		@query 			  = parsed_target_url.query
		@fragment 		  = parsed_target_url.fragment
		
		self
	end

	def parse_target_url
		query_parameters = parse_query_parameters
		target = ""
		if referrer.include?('google')
			target = query_parameters["url"]
		elsif referrer.include?('bing')
			target = query_parameters["sk"]
		end

		URI.unescape(URI.unescape(target))
	end

	def parse_query_parameters		
		query_parameters = {}
		referrer_query.split('&').each do |query_params|
			key, value = query_params.split("=")
			query_parameters[key] = value
		end

		query_parameters
	end
end